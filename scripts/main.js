/**
 * Main init of application
 */
var noOfDays = 0;
var noOfShifts = 0;
// Add the first week
addWeek();

createAddWeekButtons();

createShiftDiv('DAG', "435", "510");
createShiftDiv('KVÄLL', "810", "150");
createShiftDiv('NATT', "1260", "-420");

createAddShiftButton();

// JQuery functions --------------------------------
$(function() {
	$(".shift").draggable({
		snap : ".weekday",
		start : handleDragEvent,
		snapMode : "inner"
	});
});
$('.weekday').droppable({
	drop : handleDropEvent
});

$(function() {
	$("#slider-range").slider(
			{
				range : true,
				min : 0,
				max : 1440,
				step : 15,
				values : [ 480, 1020 ],
				slide : function(event, ui) {
					setTimeLabel(ui.values[0],ui.values[1]);
				}
			});
	setTimeLabel($("#slider-range").slider("values", 0),$("#slider-range").slider("values", 1));
});

// Handle event functions
function handleDragEvent(event, ui) {
	$(this).data('id', event.target.getAttribute('id'));
}
function handleDropEvent(event, ui) {
	var draggedItem = document.getElementById(ui.draggable.data('id'));
	event.target.setAttribute("before", draggedItem.getAttribute('before'));
	event.target.setAttribute("after", draggedItem.getAttribute('after'));
	event.target.setAttribute("work", "yes");
	event.target.setAttribute("class", "square shift");
	event.target.style.cursor= "auto"
	$("#" + event.target.id).html(draggedItem.innerHTML);
	
	/*var hoursTD = $("#"+event.target.getAttribute("id")).parent().siblings(".workHours");
	var dayWorkMinutes = 24*60 - draggedItem.getAttribute('before') -  draggedItem.getAttribute('after');
	var workMinutes = dayWorkMinutes +parseInt(hoursTD.attr("workMinutes"));
	hoursTD.attr("workMinutes",workMinutes);
	hoursTD.html(timeConvert(workMinutes));*/

	// Add clean day scheduel button
	var resetButton = document.createElement('a');
	resetButton.setAttribute("href", "#");
	resetButton.setAttribute("class", "close");
	event.target.appendChild(resetButton);
	resetButton.addEventListener('click', function() {
		event.target.setAttribute("work", "no");
		event.target.setAttribute("before", 1440);
		event.target.setAttribute("after", 1440);
		$("#" + event.target.id).html(event.target.getAttribute('name'));
		event.target.setAttribute("class", "square weekday");
		/*var newMinutes = parseInt(hoursTD.attr("workMinutes"))-dayWorkMinutes;
		hoursTD.attr("workMinutes",newMinutes);
		hoursTD.html(timeConvert(newMinutes));*/
		evaluate();
	});
	draggedItem.style.top = 'auto';
	draggedItem.style.left = 'auto';	
	evaluate();
}

//Eventlistners
document.getElementById("addButton").addEventListener('click', function() {
	var name = $("#nameInput").val();
	if(name==""){
		name="PASS"+(noOfShifts+1)
	}
	$("#addNewShiftButton").remove();
	createShiftDiv(name, $("#slider-range").slider(
			"values", 0), 1440 - $("#slider-range").slider("values", 1));
	$(function() {
		$("#shift"+noOfShifts).draggable({
			snap : ".weekday",
			start : handleDragEvent,
			snapMode : "inner"
		});
	});
	createAddShiftButton();
	$("#addShiftForm").hide();
	$("#shifts").show();
	
});
document.getElementById("backButton").addEventListener('click', function() {
	$("#addShiftForm").hide();
	$("#shifts").show();
});

document.getElementById("radioDay").addEventListener('click', function() {
	$("#slider-range").slider("option","min",0);
	$("#slider-range").slider("option","max",1440);
	$("#slider-range").slider("values",0,480);
	$("#slider-range").slider("values",1,1020);
	$("#timeLabelTD").html(
			getTime($("#slider-range").slider("values", 0)) + " - "
					+ getTime($("#slider-range").slider("values", 1)));
});
document.getElementById("radioNight").addEventListener('click', function() {
	$("#slider-range").slider("option","min",720);
	$("#slider-range").slider("option","max",2160);
	$("#slider-range").slider("values",0,1260);
	$("#slider-range").slider("values",1,1800);
	$("#timeLabelTD").html(
			getTime($("#slider-range").slider("values", 0)) + " - "
					+ getTime($("#slider-range").slider("values", 1)));
});