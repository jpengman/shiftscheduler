/**
 * Functions for modifying the calendar
 */
function addWeek() {
	var newRow = document.createElement("tr");
	var weekCell = document.createElement("td");
	var hoursCell = document.createElement("td");
	var weekNo = (noOfDays / 7) + 1
	weekCell.innerHTML = weekNo;
	hoursCell.setAttribute("workMinutes",0);
	hoursCell.setAttribute("class","workHours");

	newRow.appendChild(weekCell);
	var weekDays = ["måndag","tisdag","onsdag","torsdag","fredag","lördag","söndag"]
	for(day in weekDays){
		addDay(weekDays[day], newRow, weekNo);
	}
	newRow.appendChild(hoursCell);
	document.getElementById('weeks').appendChild(newRow);
	$('.weekday').droppable({
		drop : handleDropEvent
	});
	if (noOfDays > 41) {
		document.getElementById("addWeekbutton").remove();
		document.getElementById("sixWeekButton").remove();
	}
}
function addDay(day, newRow, weekNo) {
	noOfDays++;
	var dayCell = document.createElement("TD");
	dayCell.setAttribute("class","dayCell")
	var dayDiv = document.createElement("DIV");
	dayDiv.setAttribute("class", "square weekday");
	dayDiv.setAttribute("id", 'day' + noOfDays);
	dayDiv.setAttribute("name", day);
	dayDiv.setAttribute("work", 'no');
	dayDiv.setAttribute("before", 1440);
	dayDiv.setAttribute("after", 1440);
	dayDiv.setAttribute("weekNo", weekNo);
	dayDiv.innerHTML = day
	dayCell.appendChild(dayDiv);
	newRow.appendChild(dayCell);
}
function createAddWeekButtons() {
	var addWeekbutton = document.createElement('button');
	addWeekbutton.appendChild(document.createTextNode("Lägg till ny vecka"));
	addWeekbutton.setAttribute("id", "addWeekbutton");
	document.getElementById('addWeekTD').appendChild(addWeekbutton);
	addWeekbutton.addEventListener('click', addWeek);
	var sixWeekButton = document.createElement('button');
	sixWeekButton.appendChild(document.createTextNode("Sex veckors schema"));
	sixWeekButton.setAttribute("id", "sixWeekButton");
	document.getElementById('sixWeeksTD').appendChild(sixWeekButton);
	sixWeekButton.addEventListener('click', function() {
		while (noOfDays < 41) {
			addWeek();
		}
	});
}