/**
 * Functions for modifying shifts
 */
function createAddShiftButton() {
	var addShiftDiv = document.createElement("DIV");
	addShiftDiv.setAttribute("id", "addNewShiftButton");
	addShiftDiv.setAttribute("class", "square addShift");
	var nameP = document.createElement("P");
	nameP.setAttribute("class", "pass");
	nameP.innerHTML = "Nytt Pass";
	addShiftDiv.appendChild(nameP);
	var timeP = document.createElement("P");
	timeP.setAttribute("class", "tid");
	timeP.innerHTML = " - ";
	addShiftDiv.appendChild(timeP);
	document.getElementById("shifts").appendChild(addShiftDiv);
	addShiftDiv.addEventListener('click', function() {
		$("#addShiftForm").show();
		$("#shifts").hide();
	});
}

function createShiftDiv(name, before, after) {
	var shiftDiv = document.createElement("DIV");
	noOfShifts++;
	shiftDiv.setAttribute("id", "shift" + noOfShifts);
	shiftDiv.setAttribute("class", "square shift");
	shiftDiv.setAttribute("before", before);
	shiftDiv.setAttribute("after", after);
	var nameP = document.createElement("P");
	nameP.setAttribute("class", "pass");
	nameP.innerHTML = name;
	shiftDiv.appendChild(nameP);
	var timeP = document.createElement("P");
	timeP.setAttribute("class", "tid");
	timeP.innerHTML = getTime(before) + "-" + getTime(1440 - after);
	shiftDiv.appendChild(timeP);
	document.getElementById("shifts").appendChild(shiftDiv);
}