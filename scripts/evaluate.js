/**
 * Function for evaluation the schema
 */
function evaluate() {
	var resulttext = '';
	var twoDayCheck = 1;
	var restCheck = 0;
	var workdays = 0;
	var days = [];
	var i = 1;
	while ($('#day' + i).length > 0) {
		days.push(document.getElementById('day' + i));
		i++;
	}

	// Check rest between work days
	for (var j = 0; j < days.length - 1; j++) {
		var rest = 0;
		rest += +(days[j].getAttribute('after'));
		rest += +(days[j + 1].getAttribute('before'));
		if (rest < 0) {
			resulttext += getSpanString(
				"Du kan inte ha ett dagpass direkt efter ett nattpass, de överlappar varandra.("
				+ formatDayWeekText(days[j])
				+ ' till '
				+ formatDayWeekText(days[j + 1]) + ")", "alert")
			restCheck = 2
		} else if ((days[j + 1].getAttribute('work') != 'no')
			&& (days[j].getAttribute('work') != 'no')) {
			var spanClass = "default";
			if (rest < 660) {
				restCheck = 1;
				spanClass = "warn";
			}
			resulttext += getSpanString('Dygnsvila ' + formatDayWeekText(days[j]) + ' - '
				+ formatDayWeekText(days[j + 1]) + ': ' + timeConvert(rest), spanClass);
		}
	}

	// Check for twoDayRest
	for (var j = 6; j < days.length; j++) {
		var restDay = 0;
		for (var k = -6; k < 0; k++) {
			if ((days[j + k].getAttribute('work') === 'no')
				&& (days[j + k + 1].getAttribute('work') === 'no')) {
				restDay++;
			}
		}
		if (restDay < 1) {
			twoDayCheck = 0;
		}
	}

	// Check for four workdays in a row
	for (var j = 0; j < days.length - 3; j++) {
		if ((days[j].getAttribute('work') === "yes")
			&& (days[j + 1].getAttribute('work') === "yes")
			&& (days[j + 2].getAttribute('work') === "yes")
			&& (days[j + 3].getAttribute('work') === "yes")) {
			workdays = +1;
		}
	}

	// Check if resultdiv exists and if not create it
	if ($('#result').length == 0) {
		// Create and add result DIV
		var resultdiv = document.createElement("DIV");
		resultdiv.setAttribute("id", "result");
		document.getElementById('content').appendChild(resultdiv);

		// Create and add resulttext DIV
		var resulttextdiv = document.createElement("DIV");
		resulttextdiv.setAttribute("id", "resultText");
		document.getElementById('result').appendChild(resulttextdiv);

		// Add clean scheduel button
		var resetButton = document.createElement('button');
		resetButton.appendChild(document.createTextNode("Rensa Schema!"));
		document.getElementById('result').appendChild(resetButton);
		resetButton.addEventListener('click', function () {
			location.reload();
		});
	}
	//Calculate weekly worktime
	var totalTime = 0;
	$(".workHours").each(function (index) {
		var weekTime = 0;
		var spanClass = "default";
		$(this).siblings(":has(div.shift)").each(function (index) {
			weekTime += (24 * 60 - parseInt($(this).children().attr("after")) - parseInt($(this).children().attr("before")));
		});
		if(weekTime>2400){
			spanClass = "warn";
		}
		if(weekTime>2600){
			spanClass = "alert";
		}
		$(this).html(getSpanString(timeConvert(weekTime),spanClass));
		totalTime += weekTime;
	});

	// Create and print the resulttext
	if (restCheck == 0) {
		resulttext += getSpanString("Dygnsvila över 11 timmar anses ge möjlighet till tillräcklig återhämtning.", "default");
	} else if (restCheck == 1) {
		resulttext += getSpanString("Om dygnsvilan är under 11 timmar är det svårt att hinna få så mycket sömn som man behöver.", "default")
	}

	if (twoDayCheck == 0) {
		resulttext += getSpanString("Det är generellt bättre att någon gång under veckan ha två dagars sammanhängande ledighet.", "warn");
	} else {
		resulttext += getSpanString("Det är bra att det finns minst två dagars sammanhängande ledighet.", "default");
		if (workdays > 0) {
			resulttext += getSpanString("Men fyra pass i rad kan vara slitsamt när man jobbar skift.", "warn");
		}
	}
	resulttext += "Total arbetstid: "+timeConvert(totalTime);
	document.getElementById('resultText').innerHTML = '<h3>Kommentar om ditt schema</h3>'
		+ resulttext;
}