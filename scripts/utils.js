/**
 *  Utility functions
 */
function getTime(before) {
	if (before > 1440) {
		before = before - 1440;
	}
	var hour = "" + Math.floor(before / 60)
	if (hour.length == 1) {
		hour = "0" + hour;
	}
	var minute = "" + before % 60;
	if (minute.length == 1) {
		minute = "00";
	}
	return hour + ":" + minute
}

function formatDayWeekText(dayDiv) {
	var text = dayDiv.getAttribute('name');
	if (noOfDays > 7) {
		text = text + " vecka " + dayDiv.getAttribute('weekNo');
	}
	return text
}

function timeConvert(n) {
	var num = n;
	var hours = (num / 60);
	var rhours = Math.floor(hours);
	var minutes = (hours - rhours) * 60;
	var rminutes = Math.round(minutes);
	return rhours + 'h' + rminutes + "min.";
}

function getSpanString(text, classString) {
	return "<span class='" + classString + "'>" + text + "</span>"
}

function setTimeLabel(from, to) {
	$("#timeLabelTD").html("Tider: " + getTime(from) + " - " + getTime(to));
}